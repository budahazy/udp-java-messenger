package messenger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ModelUDP implements MessengerModel {

    ListenerModel listener;
    DatagramSocket socket;
    ExecutorService executor;
    Future<Void> future;
    InetSocketAddress socketAdress;
    volatile boolean timeToStop;

    public ModelUDP(ListenerModel listener, int port) {
        this.listener = listener;
        this.timeToStop = false;
        try {
            this.socket = new DatagramSocket(port);
            this.socket.setSoTimeout(1000);
            this.executor = Executors.newCachedThreadPool();
            this.future = executor.submit(new MessageListener());
        } catch (SocketException ex) {
            listener.errorHappened(ex.getMessage());
        }

    }

    @Override
    public boolean connect(String host, int port) {
        try {
            socketAdress = new InetSocketAddress(host, port);
            return true;
        } catch (Exception e) {
            listener.errorHappened(e.getMessage());
            return false;
        }
    }

    @Override
    public void send(String message) {
        byte[] messageData = message.getBytes(StandardCharsets.UTF_8);
        DatagramPacket dataPacket = new DatagramPacket(messageData, messageData.length, socketAdress.getAddress(), socketAdress.getPort());
        try {
            socket.send(dataPacket);
        } catch (IOException ex) {
            listener.errorHappened(ex.getMessage());
        }
    }

    @Override
    public void close() {
        timeToStop = true;
        try {
            future.get();
        } catch (InterruptedException | ExecutionException ex) {
            listener.errorHappened(ex.getMessage());
        }
        executor.shutdown();
        socket.close();
    }

    private class MessageListener implements Callable<Void> {

        byte[] recivedMessage;
        DatagramPacket dataPacket;

        public MessageListener() {
            recivedMessage = new byte[2000000];
            dataPacket = new DatagramPacket(recivedMessage, recivedMessage.length);
        }

        @Override
        public Void call() {
            while (!timeToStop) {
                try {
                    socket.receive(dataPacket);
                    String message = new String(dataPacket.getData(), 0, dataPacket.getLength(), StandardCharsets.UTF_8);
                    listener.messageRecived(message);
                } catch (SocketTimeoutException ex) {
                    continue;
                } catch (IOException ex) {
                    listener.errorHappened(ex.getMessage());
                }
            }
            return null;
        }
    }
}
