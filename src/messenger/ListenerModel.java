package messenger;

public interface ListenerModel {

    void messageRecived(final String message);

    void errorHappened(final String message);

}
